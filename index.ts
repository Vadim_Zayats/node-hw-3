import { FileDB, InputData } from "./service/crud-db-service";

const fileDB = new FileDB();

const newspostSchema = {
  id: Number,
  title: String,
  text: String,
  createDate: Date,
};

export const data: InputData[] = [
  { title: "Назва 1", text: "Текст 1" },
  { title: "Назва 2", text: "Текст 2" },
];

(async () => {
  fileDB.registerSchema("newsposts", newspostSchema);
  const newspostTable = fileDB.getTable("newsposts");
  console.log("create(): ", await newspostTable.create(data));
  console.log("getAll(): ", await newspostTable.getAll());
  console.log("getById(): ", await newspostTable.getById(1));
  console.log(
    "update(): ",
    await newspostTable.update(1, {
      text: "Змінений текст",
    })
  );
  console.log("delete(): ", await newspostTable.delete(1));
})();
