import * as fsPromise from "fs/promises";

type Data = {
  id: number;
  title: string;
  text: string;
  createDate: Date;
};

type Schema = {
  [key: string]: any;
};

export type InputData = {
  title: string;
  text: string;
};

export class FileDB {
  schemas: { [key: string]: Schema } = {};

  constructor() {}

  async registerSchema(tableName: string, schema: Schema): Promise<void> {
    this.schemas[tableName] = schema;
    const filePath = `${tableName}.json`;
    try {
      await fsPromise.writeFile(filePath, JSON.stringify([], null, 2)); // Пустий масив початкових даних
      console.log(`Створено файл бази даних ${tableName}`);
    } catch (error) {
      console.error(
        `Помилка при створенні файлу бази даних ${tableName}:`,
        error
      );
    }
  }

  async saveData(tableName: string, data: Data[]): Promise<void> {
    const filePath = `${tableName}.json`;
    try {
      await fsPromise.writeFile(filePath, JSON.stringify(data, null, 2));
      console.log(`Дані збережено у файлі ${filePath}`);
    } catch (error) {
      console.error(`Помилка при збереженні даних у файлі ${filePath}:`, error);
    }
  }

  async readFile(): Promise<Data[] | undefined> {
    try {
      const data = await fsPromise.readFile("newsposts.json", "utf-8");
      if (data !== undefined) {
        return JSON.parse(data);
      } else {
        console.log("Помилка: не вдалося прочитати файл.");
      }
    } catch (error) {
      console.error(error);
    }
  }

  getTable(tableName: string) {
    return {
      create: async (dataArray: InputData[]): Promise<Data[]> => {
        const newDataWithIds = dataArray.map((data, index) => ({
          id: new Date().getTime(),
          // id: index,
          ...data,
          createDate: new Date(),
        }));
        this.saveData(tableName, newDataWithIds);
        return newDataWithIds;
      },

      getAll: async (): Promise<Data[] | undefined> => {
        return this.readFile();
      },
      getById: async (id: number): Promise<Data | undefined> => {
        const allData = await this.readFile();
        if (allData !== undefined) {
          return allData.find((item: Data) => item.id === id);
        } else {
          return undefined;
        }
      },

      update: async (
        id: number,
        newData: Partial<Data>
      ): Promise<Data | string | undefined> => {
        let allData = await this.readFile();
        if (allData !== undefined) {
          const dataIndex = allData.findIndex(
            (item: Data | undefined) => item && item.id === id
          );
          if (dataIndex !== -1 && allData[dataIndex]) {
            allData[dataIndex] = {
              ...allData[dataIndex],
              ...newData,
            };
            this.saveData(tableName, allData);
            return allData[dataIndex];
          }
          return "Такого посту не існує";
        }
      },
      delete: async (
        id: number
      ): Promise<string | number | undefined | null> => {
        let allData = await this.readFile();
        if (allData !== undefined) {
          const indexToDelete = allData.findIndex(
            (item: Data | undefined) => item && item.id === id
          );
          if (indexToDelete !== -1) {
            const deletedId = allData[indexToDelete].id;
            allData = allData.filter(
              (item: Data | undefined) => item && item.id !== id
            );
            this.saveData(tableName, allData);
            console.log(`Пост з id "${id}" видалено`);
            return deletedId;
          }
          console.log(`Посту з id "${id}" не існує`);
          return null;
        }
      },
    };
  }
}
